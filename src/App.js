import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";

import LoginForm from './components/LoginForm/LoginForm';
import RegistrationForm from './components/RegistrationForm/RegistrationForm';
import history from './history';
import Home from './components/Home/Home';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route
// } from "react-router-dom";
// import AlertComponent from './components/AlertComponent/AlertComponent';  
export default class Routes extends Component {
  render() {
      return (
          <Router history={history}>
              <Switch>
                  <Route path="/" exact component={RegistrationForm} />
                  <Route path="/login" component={LoginForm} />
                  <Route path="/home" component={Home} />
                  {/* <Route path="/Products" component={Products} /> */}
              </Switch>
          </Router>
      )
  }
}
// function App() {
//   const [title, updateTitle] = useState(null);
//   const [errorMessage, updateErrorMessage] = useState(null);
//   return (
//     <Router>
//     <div className="App">
//       <Header title={title}/>
//         <div className="container d-flex align-items-center flex-column">
//           <Switch>
//           <Router history={history}>
//                 <Switch>
//                     <Route path="/" exact component={Home} />
//                     <Route path="/About" component={log} />
//                     <Route path="/Contact" component={Contact} />
//                     <Route path="/Products" component={Products} />
//                 </Switch>
//             </Router>
//             {/* <Route path="/" exact={true}>
//               <RegistrationForm showError={updateErrorMessage} updateTitle={updateTitle}/>
//             </Route>
//             <Route path="/register">
//               <RegistrationForm showError={updateErrorMessage} updateTitle={updateTitle}/>
//             </Route>
//             <Route path="/login">
//               <LoginForm showError={updateErrorMessage} updateTitle={updateTitle}/>
//             </Route>
//             <Route path="/home">
//               <Home/>
//             </Route> */}
//           </Switch>
//           <AlertComponent errorMessage={errorMessage} hideError={updateErrorMessage}/>
//         </div>
//     </div>
//     </Router>
//   );
// }

// export default App;
