import React, { Component } from "react";
import axios from "axios";
import history from '../../history';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import {API_BASE_URL} from '../../constants/apiContants';

export default class Login extends Component {
  constructor(props) {
    super(props);
    // this.state={myName: ""}
    this.state = {
      username:"",
      email: "",
      password: "",
      loginErrors: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { email, password } = this.state;
    var payload={
      "username":email,
	    "password":password
    }
    axios
      .post(
        API_BASE_URL+'login',payload
      )
      .then(response => {
          console.log(response);
        if (response.data.code == 200) {
          this.setState({username: response})
          // Session.setPersistent({name: this.state.name, email: this.state.email});
          sessionStorage.setItem('UserName',response);
          sessionStorage.setItem('UserName', this.state.email);
          // this.props.handleSuccessfulAuth(response.data);
          history.push('/home');
        }
             else if(response.data.code == 204){
       console.log("Username password do not match");
       alert(response.data.success)
     }
      })
      .catch(error => {
        console.log("login error", error);
      });
    event.preventDefault();
  }

  render() {
    return (
        <MuiThemeProvider>
      <div>
           
         
            <AppBar
           title="Login"
         />
         <br/>
        <form onSubmit={this.handleSubmit}>
          <TextField
            // type="email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChange}
            required
          /><br/>
          <TextField
               type="password"
               name="password"
               placeholder="Password"
               value={this.state.password}
               onChange={this.handleChange}
               required
               />
             <br/>
        
             <br/>
          <RaisedButton type="submit">Login</RaisedButton>
        </form>
        
      </div></MuiThemeProvider>
    );
  }
}

