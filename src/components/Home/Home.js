import axios from "axios";
import {API_BASE_URL} from '../../constants/apiContants';
import React, { Component, Fragment } from "react";
import { Select, DatePicker,  Form, Button, Table, Input ,Row, Col, Tooltip,message} from "antd";

import { Link } from "react-router-dom";

// import App from "../../App";
import "../../../node_modules/antd/dist/antd.css";
var moment = require("moment");
const Option = Select.Option;
const FormItem = Form.Item;


 

const data = [
  {
    userid: "1",
    controller_name: "Gym"
  }
];
class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
        users: [],
        selectedId: '',
        stationname:'',
        isLoaded:false,
        subStartDate: "",
        subEndDate: "",
        task:"",
        dateStrings:true,
        data1: [],
    }
    this.handleChange = this.handleChange.bind(this);

  }
  columns = [
 
  {
    title: 'Task Id',
    dataIndex: 'taskId',
    key: 'taskId',
  },
  {
    title: 'Task',
    dataIndex: 'task',
    key: 'task',
  }
  ];
  handledate = (date, dateString) => {
    this.setState({ subStartDate: dateString });
  };
handleChange(event) {
    console.log('Submited val ::',event.target.value);
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  submitTask= (date, dateString) => {
   this.setState({ subEndDate: dateString });
};
  getData= () => {
         const data=sessionStorage.getItem('UserName');
    // console.log("DATA",sessionStorage.getItem('UserName'));
    var payload={
        "task": this.state.task,
        "username":data
        }
        console.log("DATA",payload);
    axios.post(API_BASE_URL+'/task', payload)
    .then(response => response.data)
    .then(json =>{
            this.makeData(json);
    })
         
  };



componentDidMount() {
    const data=sessionStorage.getItem('UserName');
    // console.log("DATA",sessionStorage.getItem('UserName'));
    var payload={
        // "task": this.state.task,
        "username":data
        }
        console.log("DATA",payload);
    axios.get(API_BASE_URL+'/getTask', payload)
    .then(response => response.data)
    .then(json =>{
            this.makeData(json);
    })
         
  };
    
  
makeData = values => {
  var data = [];
 values.forEach(element => {

    let text = ''


    // console.log('test-1 :', (moment(element.for_date)).format("DD-MM-YYYY"));
    let obj = {
        username:element.username,
        taskId: element.taskId,
        task: element.task
  
    }

      data.push(obj);
    
  });

  this.setState({data1: data})


};

onChangeVal = (val)=>{

  this.setState({selectedId: val})


}

  render() {

   var{users,editUser,Id,stationname}=this.state;
   const { data1 ,values} = this.state;

    return (
      <div>
        <Fragment>
          <h1>HOME</h1>
        </Fragment>
        <Row>
        
        <Col span={5}>
        <FormItem label="Task">
                <Input 
                name="task"
             placeholder="Task"
              value={this.state.task}
                onChange={this.handleChange}/> 
                </FormItem>
        </Col>
        
           <Col >
          <Button
           type="primary"
           htmlType="submit"
           style={{ margin: "40px" }}
           onClick={this.getData }>
                    SAVE
           </Button>
                
           
           </Col>
            </Row>
           
            <Table   columns={this.columns}
              dataSource={data1}
              pagination={true} />

      </div>
    
    );
  }
}
export default Home;
